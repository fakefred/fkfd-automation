import os
import json
from datetime import datetime
from subprocess import run, CalledProcessError
from atom import gen_atom
from toot import toot
from ssg import comic_page_ssg, archive_page_ssg, index_page_ssg
from config import *
from utils import *


def submit_new():
    num = latest_comic_num() + 1
    today = datetime.now().date()

    print("New comic: #" + str(num))

    submission_form = f"""### New comic: # {str(num)} (one line only for each field)
### Title:

### Alt text:

### Image filename (without extension, leave blank for auto conversion):

"""

    # create a temporary file for metadata submission
    tmp_fn = f"/tmp/fkfd_{num}"
    try:
        with open(tmp_fn, "x") as f:
            f.write(submission_form)
            f.close()
    except FileExistsError:
        # previous run probably failed, retain data
        pass

    run(["nvim", tmp_fn])
    meta = {}

    f = open(tmp_fn)
    fields = f.readlines()
    title, alt, img = tuple(
        [l.rstrip("\n") for l in filter(lambda s: not s.startswith("### "), fields)]
    )
    img = img or image_filename(title)
    png_fn = f"{img}.png"
    kra_fn = f"{img}.kra"
    meta = {
        "title": title,
        "safe_title": title,
        "num": num,
        "year": today.year,
        "month": today.month,
        "day": today.day,
        "iso_date": today.isoformat(),
        "author": "fkfd",
        "alt": alt,
        "img": png_fn,
        "news": "",
        "link": "",
    }
    f.close()

    # check if the image is there
    comic_dir = OUTPUT_DIR / str(num)
    files = os.listdir()
    if png_fn not in files:
        print(f"{png_fn} not found. Abort.")
        exit(1)

    # mkdir html/<num> and move comic inside
    os.mkdir(comic_dir)
    os.rename(png_fn, comic_dir / png_fn)
    try:
        os.rename(kra_fn, comic_dir / kra_fn)
    except FileNotFoundError:
        print("Warning: krita file not found")

    try:
        # remove krita autosave file
        os.remove(kra_fn + "~")
    except FileNotFoundError:
        pass

    # write into meta file
    with open(comic_dir / "info.0.json", "w") as f:
        json.dump(meta, f)
        f.close()

    # ssg
    comic_page_ssg(num)
    archive_page_ssg()
    index_page_ssg()

    print("Generating Atom feed")
    gen_atom(
        num,
        title,
        alt,
        f"https://fkfd.me/comics/{num}/{png_fn}",
    )
