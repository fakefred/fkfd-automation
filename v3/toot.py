from mastodon import Mastodon
from config import MASTO_ACCESS_TOKEN, MASTO_CLIENT_KEY, MASTO_CLIENT_SECRET


def toot(meta):
    masto = Mastodon(
        client_id=MASTO_CLIENT_KEY,
        client_secret=MASTO_CLIENT_SECRET,
        access_token=MASTO_ACCESS_TOKEN,
        api_base_url="https://mastodon.technology",
    )

    try:
        media_id = masto.media_post(
            f'archive/{meta["num"]}/{meta["img"] + ".png"}',
            focus=(-1, 1),
            description=meta["alt"],
            mime_type="image/png",
        )["id"]

        masto.status_post(
            f'#fkfd | {meta["title"]} \n'
            + f'title text: {meta["alt"]} \n\n'
            + f"https://fkfd.me/{meta['num']} \n",
            media_ids=[media_id],
            visibility="public",
        )
    except FileNotFoundError:
        print("Image not found.")
        exit(1)
