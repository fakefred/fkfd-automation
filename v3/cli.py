#!/usr/bin/python
"""
CLI for fkfd comics.
You can:
- Submit a new comic (use --new flag)
- Re-generate the static site for all existing comics (use --all flag)
- Deploy comics to production server (use --deploy flag)
"""

import os
import subprocess
from argparse import ArgumentParser
from ssg import comic_page_ssg, archive_page_ssg, index_page_ssg
from new import submit_new
from atom import gen_atom
from utils import list_comics_nums
from config import *

parser = ArgumentParser("fkfd")
parser.add_argument("--new", action="store_true")
parser.add_argument("--number", type=int)
parser.add_argument("--all", action="store_true")
parser.add_argument("--deploy", action="store_true")

args = parser.parse_args()

if args.new:
    """
    Workflow:
    - draw comic
    - save comic to ./<title>.kra and .png
    - run this script with --new
    """
    submit_new()

if args.number:
    comic_page_ssg(args.number)
    archive_page_ssg()
    index_page_ssg()

if args.all:
    comics = list_comics_nums()
    for c in comics:
        try:
            comic_page_ssg(int(c))
        except ValueError:
            print(f"WARNING: comic page ssg failed in directory {c}")

    archive_page_ssg()
    index_page_ssg()

if args.deploy:
    subprocess.run(
        "rsync -rv "
        + str(OUTPUT_DIR / "*")
        + " "
        + f"{SERVER_USER}@{SERVER_HOST}:{SERVER_PATH}",
        shell=True,
    )

    try:
        subprocess.run(["scp", "atom.xml", "www@fkfd.me:/var/www/feed/atom.xml"])
    except subprocess.CalledProcessError:
        print("Error uploading Atom feed")
