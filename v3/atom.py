from feedgen.feed import FeedGenerator
from feedgen.entry import FeedEntry


def gen_atom(id: int, title: str, hover: str, image_src: str):
    fg = FeedGenerator()
    fg.title("fkfd")
    fg.link({"href": "https://fkfd.me/comics/"})
    fg.id("https://fkfd.me/comics/")
    fg.icon("https://fkfd.me/favicon.ico")

    entry = FeedEntry()
    entry.title(f"fkfd comic — {title}")
    entry.link({"href": f"https://fkfd.me/comics/{id}"})
    entry.id(f"https://fkfd.me/comics/{id}")
    entry.summary(
        f'<img src="{image_src}" title="{hover}" alt="{hover}" />', type="html"
    )

    fg.add_entry(entry)
    fg.atom_file("atom.xml", pretty=True)
