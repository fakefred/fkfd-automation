#!/usr/bin/python3
import os
import shutil
import json
import html
from config import *
from utils import *

# read all templates
comic_template_file = open(TEMPLATE_DIR / "comic.html")
comic_template = comic_template_file.read()
comic_template_file.close()

archive_template_file = open(TEMPLATE_DIR / "archive.html")
archive_template = archive_template_file.read()
archive_template_file.close()


def fill_template(template: str, data: dict) -> str:
    for keyword, value in data.items():
        template = template.replace(r"{{" + keyword + r"}}", html.escape(value))
    return template


def comic_page_html(num: int) -> str:
    meta = load_meta(num)

    html_string = fill_template(
        comic_template,
        {
            "title": meta["title"],
            "date": meta["iso_date"],
            "author": meta["author"],
            "img": f"/comics/{meta['num']}/{meta['img']}",
            "alt": meta["alt"],
            "news": meta["news"],
            "prev": str(num - 1) if num > 1 else "1",
            "next": str(num + 1) if num < latest_comic_num() else str(num),
            "quote": get_quote(),
        },
    )

    return html_string


def comic_page_ssg(num: int):
    try:
        os.mkdir(OUTPUT_DIR / str(num))
    except FileExistsError:
        pass

    with open(OUTPUT_DIR / str(num) / "index.html", "w") as f:
        f.write(comic_page_html(num))
        f.close()


def archive_page_html() -> str:
    comics = list_comics_nums()
    comics.sort(key=int, reverse=True)
    item_template = r'<a href="/comics/{{num}}">{{num}}: {{title}}</a><br />' + "\n"
    list_html = ""
    for c in comics:
        try:
            meta = load_meta(c)
            list_html += fill_template(
                item_template,
                {
                    "num": str(meta["num"]),
                    "title": meta["title"],
                },
            )
        except FileNotFoundError:
            print(f"WARNING: metadata not found in directory {c}")
    # cannot use fill_template because list_html is already html and must not be escaped
    html_string = archive_template.replace(r"{{list}}", list_html)
    return html_string


def archive_page_ssg():
    with open(OUTPUT_DIR / "archive.html", "w") as f:
        f.write(archive_page_html())
        f.close()


def index_page_ssg():
    latest = latest_comic_num()
    shutil.copy(OUTPUT_DIR / str(latest) / "index.html", OUTPUT_DIR / "index.html")
    shutil.copy(OUTPUT_DIR / str(latest) / "info.0.json", OUTPUT_DIR / "info.0.json")
