import os
import re
import json
from random import choice
from config import COMICS_DIR


def list_comics_nums() -> list:
    return list(filter(lambda s: s.isdecimal(), os.listdir(COMICS_DIR)))


def latest_comic_num() -> int:
    dirs = list_comics_nums()
    count = len(dirs)
    if str(count + 1) in dirs:
        return count + 1
    else:
        return count


def get_quote() -> str:
    with open("quotes.txt") as f:
        quotes = f.readlines()
        f.close()
        return choice(quotes)


def load_meta(num: int) -> dict:
    meta_file = open(COMICS_DIR / str(num) / "info.0.json")
    meta = json.load(meta_file)
    meta_file.close()
    return meta


def image_filename(title: str) -> str:
    return re.sub(
        "[',.?!]", "", re.sub(r"[\W_/]", "-", re.sub(" & ", "-and-", title))
    ).lower()
