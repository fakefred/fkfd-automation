#!/usr/bin/node
const chalk = require('chalk')
const inquirer = require('inquirer')
const fs = require('fs-extra')

const date = new Date()
// const id = fs.readdirSync('./public/comics/').length + 1

const config = fs.readJSONSync('./config.json')
config.recent++
const id = config.recent

const generateSafeTitle = original => {
    const safe = original
        .toLowerCase()
        .replace("'", '')
        .replace(/\W/g, '-')
        .replace(/[,.?!]/g, '')
        .replace(/[_/]/g, '-')
        .replace(' & ', '-and-')
    return safe
}

const questions = [
    {
        type: 'input',
        name: 'name',
        message: 'Title:'
    },
    {
        type: 'number',
        name: 'year',
        message: 'Year:',
        default: date.getFullYear()
    },
    {
        type: 'number',
        name: 'month',
        message: 'Month:',
        default: date.getMonth() + 1, // date.getMonth() is zero-indexed
        validate: input => {
            // is a valid month
            if (input >= 1 && input <= 12) return true
            return 'Invalid month'
        }
    },
    {
        type: 'number',
        name: 'date',
        message: 'Date:',
        default: date.getDate(),
        validate: input => {
            if (input >= 1 && input <= 31) return true
            return 'Invalid date'
        }
    },
    {
        type: 'input',
        name: 'author',
        message: 'Author:',
        default: Object.keys(fs.readJSONSync('./config.json').users)[0]
    },
    // file a bug report if you happen to live on Mars
    {
        type: 'input',
        name: 'hover',
        message: 'Hover:'
    },
    {
        type: 'text',
        name: 'tags',
        message: 'Tags(separate with comma): '
    },
    {
        type: 'input',
        name: 'image',
        message: 'Image filename:',
        default: '${SAFE_TITLE}.png'
    }
]

console.info(chalk.bold.green(`## You are uploading comic #${id} ##`))
inquirer.prompt(questions).then(answers => {
    const safeTitle = generateSafeTitle(answers.name)
    answers.image = answers.image.replace('${SAFE_TITLE}', safeTitle)
    answers.tags = answers.tags.split(', ')
    console.info(chalk.bold.bgGreen('Please review metadata: \n'))
    Object.keys(answers).forEach(field => {
        console.info(chalk.yellow(field + ': ') + answers[field])
    })
    inquirer
        .prompt([
            {
                type: 'confirm',
                name: 'confirm',
                message: 'Confirm? '
            }
        ])
        .then(fb => {
            if (fb.confirm) {
                fs.mkdirp(`./${id}`).then(() => {
                    fs.writeJSON(`./${id}/meta.json`, {
                        id,
                        name: answers.name,
                        year: answers.year,
                        month: answers.month,
                        date: answers.date,
                        author: answers.author,
                        hover: answers.hover,
                        tags: answers.tags,
                        image: answers.image
                    }).then(() => {
                        fs.writeJSONSync('./config.json', config)
                    })
                })
            }
        })
})
