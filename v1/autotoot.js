#!/usr/bin/node

const debug = false

const fs = require('fs-extra')

const Mastodon = require('mastodon')
const bot = new Mastodon({
    access_token: '',
    api_url: 'https://mastodon.technology/api/v1/'
})

const files = fs.readdirSync('./')
let dir = 0
files.forEach(item => {
    if (/^\d+$/.test(item)) dir = parseInt(item)
})

if (dir !== 0) {
    console.info(
        `FOUND TARGET: ${dir.toString()}. ACTION WILL BE EXECUTED IN 5 SECONDS.`
    )
    const meta = fs.readJSONSync(`./${dir.toString()}/meta.json`)
    setTimeout(() => {
        bot.post('media', {
            file: fs.createReadStream(`./${dir.toString()}/${meta.image}`),
            description: meta.hover,
            focus: '-1,1' // upper-left corner
        })
            .then(res => {
                console.info('IMAGE UPLOADED; TOOTING')

                bot.post('statuses', {
                    status:
                        `${meta.name} \n` +
                        '#oc #comic made with #krita \n\n' +
                        `alt text: ${meta.hover} \n\n` +
                        `https://fkfd.me/${meta.id} \n` +
                        (debug ? '@fakefred' : ''),
                    media_ids: [res.data.id],
                    visibility: debug ? 'direct' : 'public',
                    sensitive: false
                }).then(resp => {
                    console.info('PUBLISHED')
                })
            })
            .catch(e => console.error(e))
    }, 5000)
} else {
    console.warn('NO TARGET FOUND. ABORT.')
}
