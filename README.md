# fkfd automation scripts

Tools I wrote and use for automated deployment of [my blog](https://fkfd.me)
and [comics](https://fkfd.me/comics). The repo is basically a personal backup.
The code is unlicensed, i.e. in the public domain.

