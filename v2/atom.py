from feedgen.feed import FeedGenerator
from feedgen.entry import FeedEntry


def gen_atom(id: int, title: str, hover: str, image_src: str):
    fg = FeedGenerator()
    fg.title("fkfd")
    fg.link({"href": "https://fkfd.me/"})
    fg.id("https://fkfd.me/")
    fg.icon("https://fkfd.me/img/favicon.png")
    entry = FeedEntry()
    entry.title(f"fkfd - {title}")
    entry.link({"href": f"https://fkfd.me/{id}"})
    entry.id(f"https://fkfd.me/{id}")
    entry.summary(
        f'<img src="{image_src}" title="{hover}" alt="{hover}" />', type="html"
    )
    fg.add_entry(entry)
    fg.atom_file("atom.xml", pretty=True)
