#!./venv/bin/python
from PyInquirer import prompt
from mastodon import Mastodon
from os import listdir, mkdir, rename
from datetime import datetime
from json import dump
from re import sub
from subprocess import run, CalledProcessError
from sys import exit
from atom import gen_atom
from fancyprint import *

id = len(list(filter(lambda f: f.isdecimal(), listdir("./archive")))) + 1
now = datetime.now()


def safe_title(title: str) -> str:
    return sub("[',.?!]", "", sub("[\W_/]", "-", sub(" & ", "-and-", title))).lower()


print(color("Metagen for comic #" + str(id), fgc=4))

try:
    ans = prompt(
        [
            {"type": "input", "name": "name", "message": "Title:"},
            {"type": "input", "name": "hover", "message": "Hover:"},
        ]
    )
except KeyError:
    exit(1)

title = safe_title(ans["name"])

try:
    filename = prompt(
        [
            {
                "type": "input",
                "name": "image",
                "message": "Image filename:",
                "default": f"{title}.webp",
            }
        ]
    )["image"]
except KeyError:
    exit(1)

mkdir(f"archive/{id}")
ans["id"] = id
ans["author"] = "fakefred"
ans["tags"] = [""]
ans["image"] = title + ".webp"
ans["year"] = now.year
ans["month"] = now.month
ans["date"] = now.day
dump(ans, open(f"archive/{id}/meta.json", "x"))

try:
    rename(f"{title}.webp", f"archive/{id}/{title}.webp")
    rename(f"{title}.kra", f"archive/{id}/{title}.kra")
    rename(f"{title}.png", f"archive/{id}/{title}.png")

except FileNotFoundError:
    print(color("File not found. It should be called `" + title + "`.", fgc=1))

try:
    run(["scp", "-r", f"archive/{id}", "node@fkfd.me:/home/node/fkfd/public/comics/"])
except CalledProcessError:
    print(color("Error uploading comic", fgc=1))

print(color("Generating Atom feed", fgc=4))
gen_atom(id, ans["name"], ans["hover"], f"https://fkfd.me/comics/{id}/{title}.png")

try:
    run(["scp", "atom.xml", "root@fkfd.me:/var/www/feed/atom.xml"])
except CalledProcessError:
    print(color("Error uploading Atom feed", fgc=1))

try:
    push = prompt(
        [
            {
                "type": "confirm",
                "name": "push",
                "message": "Toot to m.t?",
                "default": False,
            }
        ]
    )["push"]
except KeyError:
    exit(1)

if push:
    masto = Mastodon(
        client_id="",
        client_secret="",
        access_token="",
        api_base_url="https://mastodon.technology",
    )

    try:
        media_id = masto.media_post(
            f'archive/{id}/{title + ".png"}',
            focus=(-1, 1),
            description=ans["hover"],
            mime_type="image/png",
        )["id"]

        masto.status_post(
            f'#fkfd | {ans["name"]} \n'
            + "#oc #comic made with #krita \n\n"
            + f'title text: {ans["hover"]} \n\n'
            + f"https://fkfd.me/{id} \n",
            media_ids=[media_id],
            visibility="public",
        )
    except FileNotFoundError:
        print(color("No png found.", fgc=1))
        exit(1)
